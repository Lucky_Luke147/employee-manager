<?php
session_start();

if((isset($_SESSION['zalogowany']))&&($_SESSION['zalogowany'] == true))
{
    header('Location: gra.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset = "utf-8" />
    <title>Employee Manager</title>
    <link rel="stylesheet" type="text/css" href="Style/css/style.css">

<body>
<div class="loginbox">
    <img src="Style/image/avatar.png" class="avatar">
    <h1>Zaloguj się</h1>


    <form action = "loginLogic.php" method = "post">
        <p>Username</p>
        <input type="text" name="login" placeholder="Enter Username">

        <p>Password</p>
        <input type="password" name="haslo" placeholder="Enter Password">
        <input type = "submit" value = "Zaloguj się" />
    </form>


    <?php
    if(isset($_SESSION['blad']))
        echo $_SESSION['blad'];

    ?>
</body>
</html>
